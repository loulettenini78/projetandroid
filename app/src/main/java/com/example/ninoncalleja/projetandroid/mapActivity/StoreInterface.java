package com.example.ninoncalleja.projetandroid.mapActivity;

import android.view.View;

import com.example.ninoncalleja.projetandroid.models.Store;

public interface StoreInterface {
    void loadData(View layout);
    void onClick(Store store);
}
