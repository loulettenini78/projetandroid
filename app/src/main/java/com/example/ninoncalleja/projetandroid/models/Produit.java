package com.example.ninoncalleja.projetandroid.models;

import android.widget.ImageView;

import java.io.Serializable;
import java.util.Date;

public class Produit implements Serializable {
    private long codeBarre;
    private String ingredients;
    private String nom;
    private String paysVente;


    public String getUrlImage() {
        return urlImage;
    }


    private String urlImage;

    public Produit(long codeBarre, String nom, String ingredients, String paysVente, String url) {
        this.codeBarre=codeBarre;
        this.ingredients=ingredients;
        this.nom=nom;
        this.paysVente=paysVente;
        this.urlImage = url;
    }
    Produit(String nom) {
        this.codeBarre= 0;
        this.ingredients=null;
        this.nom=nom;
        this.paysVente=null;
        this.urlImage =null;

    }


    public long getCodeBarre() {
        return codeBarre;
    }


    public String getIngredients() {
        return ingredients;
    }


    public String getPaysVente() {
        return paysVente;
    }


    public String getName() {
        return nom;
    }



}
