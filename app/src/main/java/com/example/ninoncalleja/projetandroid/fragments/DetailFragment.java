package com.example.ninoncalleja.projetandroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ninoncalleja.projetandroid.mainActivity.DetailInterface;
import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.models.Produit;
import com.squareup.picasso.Picasso;


public class DetailFragment extends Fragment {

    private DetailInterface mListener;
    private Produit produit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_detail, container, false);

        //Recuperation of data from AccueilActivity
        Bundle bundle = getArguments();
        produit = (Produit) bundle.getSerializable("produit");

        //Init of the composant of the layout
        TextView nom_produit = layout.findViewById(R.id.nom_produit_detail);
        TextView ingredients = layout.findViewById(R.id.liste_ingredients_detail);
        TextView code = layout.findViewById(R.id.code_detail);

        //For scrollbar
        ingredients.setMovementMethod(new ScrollingMovementMethod());

        //Verify that the url isn't empty
        if (produit.getUrlImage()!="")
        {
            ImageView ivBasicImage =layout.findViewById(R.id.image_front_detail);
            Picasso.with(getContext()).load(produit.getUrlImage()).into(ivBasicImage);
        }
        //Define a default object it needed
        else
        {
            ImageView ivBasicImage = layout.findViewById(R.id.image_front_detail);
            ivBasicImage.setImageResource(R.drawable.button_accueil);
        }
        //display details on products
        nom_produit.setText(produit.getName());
        ingredients.setText((produit.getIngredients()));
        code.setText(String.valueOf(produit.getCodeBarre()));

        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DetailInterface) {
            mListener = (DetailInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
