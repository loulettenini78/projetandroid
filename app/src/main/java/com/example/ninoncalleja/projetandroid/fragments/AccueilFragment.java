package com.example.ninoncalleja.projetandroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.ninoncalleja.projetandroid.mainActivity.AccueilInterface;
import com.example.ninoncalleja.projetandroid.R;


public class AccueilFragment extends Fragment {

    private AccueilInterface mListener;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_accueil, container, false);

        //Init for view composants
        ImageButton button_accueil = layout.findViewById(R.id.button_accueil);

        //On click for button_accueil
        button_accueil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mListener.recherche_produit();

            }
        });
            return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AccueilInterface) {
            mListener = (AccueilInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
