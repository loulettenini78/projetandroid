package com.example.ninoncalleja.projetandroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.mainActivity.SearchInterface;
import com.example.ninoncalleja.projetandroid.models.ListeProduits;

import java.io.IOException;


public class SearchFragment extends Fragment {

    //variables
    private SearchInterface mListener;
    private String URL = "https://fr.openbeautyfacts.org/cgi/search.pl?search_terms=";
    private String URL_Json = "&search_simple=1&action=process&xml=1";
    private ListeProduits liste = new ListeProduits();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_search, container, false);

        //Definition of the view composants
        Button button_search = layout.findViewById(R.id.button_search);
        EditText text_search = layout.findViewById(R.id.editText_search);

        //Load data
        mListener.loadData(layout);
        //on click action for button search
        button_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Verify that the user has entered text in the TextEdit
                String nomProduit = text_search.getText().toString();
                if (TextUtils.isEmpty(nomProduit))
                {
                    showToast();
                }
                else
                {
                    try {
                        mListener.searchProduct(nomProduit,liste,layout);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        return layout;
    }

    void showToast() {
        Toast.makeText(getContext(), "Renseigner le nom du produit..", Toast.LENGTH_LONG).show();

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SearchInterface) {
            mListener = (SearchInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    }




