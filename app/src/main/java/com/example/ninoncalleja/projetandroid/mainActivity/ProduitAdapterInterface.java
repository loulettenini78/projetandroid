package com.example.ninoncalleja.projetandroid.mainActivity;

import com.example.ninoncalleja.projetandroid.models.Produit;

//for homemade click listener
public interface ProduitAdapterInterface {
    void onClick(Produit produit);
}
