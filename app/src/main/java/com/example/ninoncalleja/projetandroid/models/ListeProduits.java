package com.example.ninoncalleja.projetandroid.models;

import java.io.Serializable;
import java.util.*;

public class ListeProduits implements Serializable {
    public ArrayList<Produit> getProduits() {
        return produits;
    }

    private ArrayList<Produit> produits = new ArrayList<>();


    public void addProduit(Produit produit) {
        this.produits.add(new Produit(produit.getCodeBarre(), produit.getName(), produit.getIngredients(), produit.getPaysVente(),produit.getUrlImage()));
    }
    public Produit getProduit(String nom){
        return this.produits.stream()
                .filter((produit) -> Objects.equals(produit.getName(), nom))
                .findFirst()
                .orElse(null);
    }

    public boolean listContains(ListeProduits liste, Produit produit)
    {
        String name = produit.getName();
        for(Produit el :liste.getProduits())
        {
            if (name.equals(el.getName()))
            {
                return true;
            }
        }
        return false;
    }

}
