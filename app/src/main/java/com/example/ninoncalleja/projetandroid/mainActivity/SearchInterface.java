package com.example.ninoncalleja.projetandroid.mainActivity;

import android.view.View;

import com.example.ninoncalleja.projetandroid.models.ListeProduits;

import java.io.IOException;

public interface SearchInterface {
    void searchProduct(String product, ListeProduits liste, View layout) throws IOException;
    void loadData(View layout);
}
