package com.example.ninoncalleja.projetandroid.mainActivity;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.models.Produit;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProduitAdapterRecherche extends RecyclerView.Adapter<ProduitViewHolderRecherche>  {
    private ArrayList<Produit> icons;
    private ProduitAdapterInterface listener;

    public ProduitAdapterRecherche(ArrayList<Produit> icons, ProduitAdapterInterface listener)
    {
        this.icons = icons;
        this.listener=listener;
    }


    public ProduitViewHolderRecherche onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.produit_liste, parent, false);
        return new ProduitViewHolderRecherche(row);
    }

    public void onBindViewHolder(ProduitViewHolderRecherche viewholder, int position) {
        viewholder.bind(this.icons.get(position),listener);
        Produit iconToDisplay = this.icons.get(position);

        viewholder.nom_produit.setText(iconToDisplay.getName());
        //Affiche l'image du produit ou une image par défaut
        if (iconToDisplay.getUrlImage()!="")
        {
            Picasso.with(viewholder.image_presentation.getContext()).load(iconToDisplay.getUrlImage()).into(viewholder.image_presentation);
        }
        else
        {
            viewholder.image_presentation.setImageResource(R.drawable.button_accueil);
        }

    }

    public int getItemCount() {
        return this.icons.size();
    }


}
