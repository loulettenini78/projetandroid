package com.example.ninoncalleja.projetandroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.mapActivity.StoreAdapter;
import com.example.ninoncalleja.projetandroid.mapActivity.StoreInterface;
import com.example.ninoncalleja.projetandroid.models.ListeStores;
import com.example.ninoncalleja.projetandroid.models.Store;

import java.util.ArrayList;

public class StoreFragment extends Fragment {
    private StoreInterface mListener;
    private String url = "https://opendata.paris.fr/explore/dataset/commercesparis/download/?format=geojson&q=beaut%C3%A9&timezone=Europe/Berlin";
    private ListeStores listStores = new ListeStores();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_store, container, false);
        //get the bundle
        Bundle bundle=getArguments();
        //Transforming the bundle from Serializable to ListeStores
        listStores= (ListeStores) bundle.getSerializable("stores");


        RecyclerView mRecyclerView = (RecyclerView) layout.findViewById(R.id.view_store);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mRecyclerView.setAdapter(new StoreAdapter(listStores.getStores()));
        System.out.println("length = "+ listStores.getStores().size());
        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof StoreInterface) {
            mListener = (StoreInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
