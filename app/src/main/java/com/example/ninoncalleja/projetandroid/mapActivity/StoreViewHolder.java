package com.example.ninoncalleja.projetandroid.mapActivity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.ninoncalleja.projetandroid.R;

public class StoreViewHolder extends RecyclerView.ViewHolder {
    public TextView type;
    public TextView adresse;

    public StoreViewHolder(View rootView)
    {
        super(rootView);

        this.type = rootView.findViewById(R.id.type);
        this.adresse = rootView.findViewById(R.id.adresse);

    }
}


