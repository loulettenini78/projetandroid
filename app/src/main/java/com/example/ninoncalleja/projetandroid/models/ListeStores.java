package com.example.ninoncalleja.projetandroid.models;

import com.example.ninoncalleja.projetandroid.models.Store;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class ListeStores implements Serializable {

    private ArrayList<Store> stores = new ArrayList<>();

    public ArrayList<Store> getStores() {
        return stores;
    }



    public void addStore(String type, String adresse) {
        this.stores.add(new Store(type,adresse));
    }

    public void addStore(Store store) {
        this.stores.add(new Store(store.getType(), store.getAdresse()));
    }
    public Store getStore(String adresse){
        return this.stores.stream()
                .filter((store) -> Objects.equals(store.getAdresse(), adresse))
                .findFirst()
                .orElse(null);
    }

   
}
