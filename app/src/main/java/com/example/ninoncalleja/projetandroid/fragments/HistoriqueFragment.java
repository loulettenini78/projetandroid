package com.example.ninoncalleja.projetandroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ninoncalleja.projetandroid.mainActivity.HistoriqueInterface;
import com.example.ninoncalleja.projetandroid.mainActivity.ProduitAdapterInterface;
import com.example.ninoncalleja.projetandroid.mainActivity.ProduitAdapterRecherche;
import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.models.ListeProduits;
import com.example.ninoncalleja.projetandroid.models.Produit;


public class HistoriqueFragment extends Fragment {

    private HistoriqueInterface mListener;

    private ListeProduits produits = new ListeProduits();

    public HistoriqueFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_historique, container, false);

        //Recuperation of data
        Bundle bundle=getArguments();
        produits= (ListeProduits) bundle.getSerializable("historique");

        //Display of the historic in the same format that the other fragments
        RecyclerView mRecyclerView= layout.findViewById(R.id.liste_historique);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        //Adapter with a click listener
        ProduitAdapterRecherche productsAdapter = new ProduitAdapterRecherche(produits.getProduits(), new ProduitAdapterInterface(){
            @Override public void onClick(Produit produit)
            {
                //Bundle to give detail fragment the clicked product
                Bundle bundle = new Bundle();
                bundle.putSerializable("produit",produit);

                //Detail fragment
                FragmentManager fragmentManager= getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                DetailFragment fragment = new DetailFragment();
                fragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        mRecyclerView.setAdapter(productsAdapter);

        return layout;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HistoriqueInterface) {
            mListener = (HistoriqueInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
