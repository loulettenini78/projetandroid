package com.example.ninoncalleja.projetandroid.mainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ninoncalleja.projetandroid.fragments.InfoFragment;
import com.example.ninoncalleja.projetandroid.mapActivity.MapActivity;
import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.fragments.AccueilFragment;
import com.example.ninoncalleja.projetandroid.fragments.DetailFragment;
import com.example.ninoncalleja.projetandroid.fragments.HistoriqueFragment;
import com.example.ninoncalleja.projetandroid.fragments.SearchFragment;
import com.example.ninoncalleja.projetandroid.models.ListeProduits;
import com.example.ninoncalleja.projetandroid.models.Produit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


public class AccueilActivity extends AppCompatActivity implements AccueilInterface, SearchInterface, DetailInterface,HistoriqueInterface,InfoFragment.OnFragmentInteractionListener {

    //Variables Fragments
    FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    //Variables Volley request on OpenBeautyFacts opendata
    private String URL = "https://fr.openbeautyfacts.org/cgi/search.pl?search_terms=";
    private String URL_Json = "&search_simple=1&action=process&json=1";
    private RequestQueue queue;
    //Variables to stock products
    private ListeProduits produitListe;
    private ListeProduits listeHistorique=new ListeProduits();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);

        //Initialisation of produitListe
        produitListe = new ListeProduits();

        //Initialisation for Volley queue
        queue = Volley.newRequestQueue(this);

        //display of the first fragment
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        AccueilFragment fragment = new AccueilFragment();
        fragmentTransaction.replace(R.id.container, fragment);
        //To return to the latest fragment when push the back button
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void recherche_produit()
    {
        //Display of the search fragment instead of the first
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SearchFragment fragment = new SearchFragment();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    //Create the menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_historique:
                //put the historic list into bundle
                Bundle bundle=new Bundle();
                System.out.println(listeHistorique.getProduits());
                bundle.putSerializable("historique",listeHistorique);

                //display the historic fragment
                fragmentTransaction = fragmentManager.beginTransaction();
                HistoriqueFragment historiqueFragment=new HistoriqueFragment();
                historiqueFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.container, historiqueFragment);
                //To return to the latest fragment when push the back button
                fragmentTransaction.commit();
                return true;

            case R.id.action_maps:
                //Create new activity to show the map and the new opendatabase
                Intent intent=new Intent(this,MapActivity.class);
                this.startActivity(intent);
                return true;

            case R.id.action_home:
                //go back to the first fragment (home)
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                AccueilFragment fragment = new AccueilFragment();
                fragmentTransaction.replace(R.id.container, fragment);
                //To return to the latest fragment when push the back button
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                return true;

            case R.id.action_Info:
                //go back to the first fragment (home)
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                InfoFragment fragment2 = new InfoFragment();
                fragmentTransaction.replace(R.id.container, fragment2);
                //To return to the latest fragment when push the back button
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                return true;
            default: // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    //Function to show the result of the research done on the search fragment
    public void searchProduct(String product, ListeProduits liste, View layout) throws IOException {
        //Reinitialisation of the list of products
        produitListe = new ListeProduits();

        //HTTP request to obtain the json file with the result of the query
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, URL + product+URL_Json,null,
                new Response.Listener<JSONObject>() {

                    //If the request works
                    @Override
                    public void onResponse(JSONObject response) {
                        showToast("Telechargement des produits ...");
                        JSONArray r = new JSONArray();
                        try {
                            //Extraction of the product list
                            r = response.getJSONArray("products");

                            //For each product of the JSONArray :
                            for (int i = 0;i<r.length();i++ )
                            {
                                //Add each product in the list of products
                                JSONObject o = r.getJSONObject(i);
                                //TODO: récupérer les autres informations utiles
                                String code = o.getString("code");
                                String name = o.getString("product_name");
                                String ingredient = o.getString("ingredients_text_debug");
                                String urlImageFront ="";
                                //Test if the property exists
                                if (o.has("image_url"))
                                {
                                    urlImageFront = o.getString("image_url");
                                }
                                Produit produit = new Produit(Long.parseLong(code),name,ingredient,"France",urlImageFront);
                                produitListe.addProduit(produit);

                                //Display in a recycler view with a homemade click listener
                                RecyclerView rView = layout.findViewById(R.id.view_search);
                                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
                                rView.setLayoutManager(layoutManager);
                                rView.setAdapter(new ProduitAdapterRecherche(produitListe.getProduits(), new ProduitAdapterInterface(){
                                    //Function to diplay a detail page if the user click on an item
                                    @Override public void onClick(Produit produit)
                                    {
                                        //Bundle to give detail fragment the clicked product
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("produit",produit);

                                        //Add into historic if the product doesn't exist already
                                        if(listeHistorique.listContains(listeHistorique,produit)==false)
                                        {
                                            listeHistorique.addProduit(produit);
                                        }

                                        //Detail fragment
                                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                        DetailFragment fragment = new DetailFragment();
                                        fragment.setArguments(bundle);
                                        fragmentTransaction.replace(R.id.container, fragment);
                                        fragmentTransaction.addToBackStack(null);
                                        fragmentTransaction.commit();
                                    }
                                }));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showToast("Requête ratée ...");
                error.printStackTrace();
            }
        });
        queue.add(stringRequest);


    }

    void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    //Load the set of data for the display of SearchFragment
    public void loadData(View layout)
    {
        //Re-initialization of the product list
        produitListe = new ListeProduits();

        //Display in recyclerview
        RecyclerView rView = layout.findViewById(R.id.view_search);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        rView.setLayoutManager(layoutManager);

        //adapter
        ProduitAdapterRecherche adapter = new ProduitAdapterRecherche(produitListe.getProduits(), new ProduitAdapterInterface() {
            @Override
            public void onClick(Produit produit) {
                //Bundle to give detail fragment the clicked product
                Bundle bundle = new Bundle();
                bundle.putSerializable("produit", produit);
                //Save in historic
                if(listeHistorique.listContains(listeHistorique,produit)==false)
                {
                    listeHistorique.addProduit(produit);
                }
                //Detail fragment
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                DetailFragment fragment = new DetailFragment();
                fragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        rView.setAdapter(adapter);

        //The set of data is divided into pages (20 products per page).
        // It's very long to get them all so we have reduce the number of requests
        for (int i=1;i<2;i++) {
            //Variables for URL
            String pageNb = String.valueOf(i);
            String url = "https://fr.openbeautyfacts.org/"+pageNb+".json";

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, null,
                    new Response.Listener<JSONObject>() {


                        @Override
                        public void onResponse(JSONObject response) {
                            //Success Callback
                            showToast("Téléchargement des données ...");
                            JSONArray r = new JSONArray();
                            try {
                                //Extraction of the product list
                                r = response.getJSONArray("products");
                                for (int i = 0; i < r.length(); i++) {
                                    //Add each product in the list of products
                                    JSONObject o = r.getJSONObject(i);
                                    //TODO: récupérer les autres informations utiles
                                    String code = o.getString("code");
                                    String name = o.getString("product_name");
                                    String ingredient = o.getString("ingredients_text_debug");
                                    String urlImageFront = "";
                                    if (o.has("image_url")) {
                                        urlImageFront = o.getString("image_url");
                                    }

                                    //add a new product into the list
                                    Produit produit = new Produit(Long.parseLong(code), name, ingredient, "France", urlImageFront);
                                    produitListe.addProduit(produit);
                                    //notify that the set of data has been modified
                                    adapter.notifyDataSetChanged();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Failure Callback
                            showToast("ratée");
                        }
                    });
            queue.add(jsonObjReq);
        }
    }
}
