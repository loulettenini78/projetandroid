package com.example.ninoncalleja.projetandroid.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ninoncalleja.projetandroid.mapActivity.MapInterface;
import com.example.ninoncalleja.projetandroid.R;

import java.util.ArrayList;


public class MarkerInfoFragment extends Fragment {

    private MapInterface mListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_marker_info, container, false);

        //Recuperation of data from MapActivity
        Bundle bundle = getArguments();
        ArrayList<String> infos = bundle.getStringArrayList("infos");

        //Init of the components of the layout
        TextView typeMagasin = layout.findViewById(R.id.type_magasin);
        TextView adresse = layout.findViewById(R.id.adresse);

        typeMagasin.setText(infos.get(0));
        adresse.setText(infos.get(1));

        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof MapInterface) {
            mListener = (MapInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}
