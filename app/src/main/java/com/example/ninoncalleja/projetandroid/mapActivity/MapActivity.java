package com.example.ninoncalleja.projetandroid.mapActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.fragments.StoreFragment;
import com.example.ninoncalleja.projetandroid.models.ListeStores;
import com.example.ninoncalleja.projetandroid.models.Store;
import com.example.ninoncalleja.projetandroid.mainActivity.AccueilActivity;
import com.google.android.gms.maps.*;

import com.android.volley.*;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.*;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,OnMarkerClickListener,MapInterface,StoreInterface {
    private GoogleMap mMap;
    private RequestQueue queue;
    private ListeStores storesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        //Initialization of the store list
        storesList = new ListeStores();

        //link mapfragment to the mmap
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //initialize the request queue
        queue = Volley.newRequestQueue(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_map, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onMapReady(GoogleMap googleMap){

        mMap=googleMap;
        // Move the camera to Paris
        LatLng paris = new LatLng(48.8534, 2.3488);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(paris));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(10f));
        mMap.addMarker(new MarkerOptions().position(paris).title("Marker in Paris"));

        String url = "https://opendata.paris.fr/explore/dataset/commercesparis/download/?format=geojson&q=beaut%C3%A9&timezone=Europe/Berlin";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {
                    //Success Callback
                    JSONArray jsonArrayFeatures=new JSONArray();
                    System.out.println(response.has("features"));
                    try {
                        jsonArrayFeatures=response.getJSONArray("features");
                        for (int i=0;i<jsonArrayFeatures.length();i++) {
                            //put each store in the list of stores
                            JSONObject jsonObjectElement=jsonArrayFeatures.getJSONObject(i);
                            JSONObject jsonObjectGeometry=jsonObjectElement.getJSONObject("geometry");
                            JSONArray jsonArrayCoordinates=jsonObjectGeometry.getJSONArray("coordinates");
                            float[] coord = fillData(jsonArrayCoordinates);
                            JSONObject jsonObjectProperties=jsonObjectElement.getJSONObject("properties");
                            String typeMagasin=jsonObjectProperties.getString("libact");
                            String adresse = jsonObjectProperties.getString("adresse_complete");
                            Store store=new Store(typeMagasin,adresse);
                            storesList.addStore(store);
                            Marker marker=mMap.addMarker(new MarkerOptions().position(new LatLng(coord[1],coord[0])).title(typeMagasin));

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Failure Callback
                        showToast("ratée");
                    }
                });
        queue.add(jsonObjReq);
    }

    void showToast(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();

    }
    //Function to transform the jsonarray into a list of floats
    private float[] fillData(JSONArray jsonArray){

        float[] fData = new float[jsonArray.length()];

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                fData[i] = Float.parseFloat(jsonArray.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return fData;
    }

    @Deprecated
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {

            case R.id.action_liste:
                Bundle bundle=new Bundle();
                bundle.putSerializable("stores",storesList);
                FragmentManager fragmentManager= getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                StoreFragment storeFragment=new StoreFragment();
                storeFragment.setArguments(bundle);
                findViewById(R.id.container).setVisibility(View.VISIBLE);

                fragmentTransaction.replace(R.id.container, storeFragment);
                //To return to the latest fragment when push the back button
                fragmentTransaction.commit();
                return true;

            case R.id.action_map:
                intent=new Intent(this,MapActivity.class);
                this.startActivity(intent);
                return true;


            case R.id.home:
                intent = new Intent(this, AccueilActivity.class);
                this.startActivity(intent);
                return true;
            default: // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void loadData(View layout) {

        RecyclerView rView = layout.findViewById(R.id.view_store);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        rView.setLayoutManager(layoutManager);

        /*StoreAdapter adapter =new StoreAdapter(storesList, new StoreInterface() {
        });*/

    }

    @Override
    public void onClick(Store store) {

    }
}

