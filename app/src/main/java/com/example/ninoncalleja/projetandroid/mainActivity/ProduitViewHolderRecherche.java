package com.example.ninoncalleja.projetandroid.mainActivity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.models.Produit;

public class ProduitViewHolderRecherche extends RecyclerView.ViewHolder {
    public ImageView image_presentation;
    public TextView nom_produit;

    public ProduitViewHolderRecherche(View rootView)
    {
        super(rootView);

        this.image_presentation = rootView.findViewById(R.id.image_search);
        this.nom_produit = rootView.findViewById(R.id.nom_produit_search);

    }

    //For homemade click listener
    public void bind(Produit produit, ProduitAdapterInterface listener) {
        nom_produit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
               listener.onClick(produit);
            }
        });

    }
}
