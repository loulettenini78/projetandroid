package com.example.ninoncalleja.projetandroid.models;

import java.io.Serializable;

public class Store implements Serializable {
        private String type;
        private String adresse;

        public Store(String type, String adresse) {
            this.type=type;
            this.adresse=adresse;
        }

        Store(String type) {
            this.type=type;
            this.adresse=null;
    }


        public String getType() {
        return type;
    }

        public void setType(String Type) {
            this.type=type;
    }

        public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse=adresse;
    }
}
