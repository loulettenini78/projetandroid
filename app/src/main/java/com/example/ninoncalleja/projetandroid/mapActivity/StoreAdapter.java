package com.example.ninoncalleja.projetandroid.mapActivity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.ninoncalleja.projetandroid.R;
import com.example.ninoncalleja.projetandroid.models.Store;

import java.util.ArrayList;

public class StoreAdapter extends RecyclerView.Adapter<StoreViewHolder> {
    private ArrayList<Store> stores;
    private StoreInterface listener;

    public StoreAdapter(ArrayList<Store> stores)
    {
        this.stores = stores;
    }

    public StoreViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View row = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.store_view_holder,viewGroup, false);
        return new StoreViewHolder(row);
    }


    public void onBindViewHolder(StoreViewHolder storeViewHolder, int i) {

        Store storeToDisplay = this.stores.get(i);
        storeViewHolder.type.setText(storeToDisplay.getType());
        storeViewHolder.adresse.setText(storeToDisplay.getAdresse());



    }


    public int getItemCount() {
        return this.stores.size();
    }
}
